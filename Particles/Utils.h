#pragma once
#include <cmath>
#include <bitset>
#include <type_traits>

namespace string_utils
{
	std::vector<std::string> split(std::string input, const std::string& delimiter);

	//note: trim functions does not modify original line so each function mean at least one copying

	// trim from start
	std::string ltrim(std::string s);

	// trim from end
	std::string rtrim(std::string s);

	// trim from both ends
	std::string trim(std::string s);

	bool starts_with(std::string const & value, std::string const & starting);

	bool ends_with(std::string const & value, std::string const & ending);

	//takes string contains float number and return float or 0 if not a number
	float stof_without_exceptions(const std::string& input);
	//takes string contains int number and return float or 0 if not a number
	int stod_without_exceptions(const std::string& input);

	bool stobool(std::string input);

	template <class T>
	std::string to_string(const T& t)
	{
		std::stringstream ss;
		ss << t;
		return ss.str();
	}
	
}

namespace file_utils
{
	std::vector<std::string> read_file(const std::string& filename);
}

namespace map_utils
{
	std::string get_const_map_value_string(const std::unordered_map<std::string, std::string>& map,const std::string& key);
	
}

namespace math_utils
{/*
	inline double to_degrees(double radians)
	{
		return radians * (180.0 / M_PI);
	}

	inline double to_radians(double degrees)
	{
		return degrees * (M_PI/180);
	}
	*/
}

namespace time_utils
{
	inline float ms_to_sec(float ms)
	{
		return ms / float(1000);
	}
}
namespace templated_helpers
{

}