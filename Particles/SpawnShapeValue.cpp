#include "SpawnShapeValue.h"
#include "Utils.h"

namespace particles
{
	//standart constructor
	SpawnShapeValue::SpawnShapeValue() : shape(SpawnShape::point), side(SpawnEllipseSide::both), edges(false){}
	//copy constructor
	SpawnShapeValue::SpawnShapeValue(const SpawnShapeValue& rhs) : ParticleValue(rhs), shape(rhs.shape),
		side(rhs.side), edges(rhs.edges){}
	//move constructor
	SpawnShapeValue::SpawnShapeValue(SpawnShapeValue&& rhs) : ParticleValue(std::move(rhs)),
		shape(std::move(rhs.shape)), side(std::move(rhs.side)), edges(std::move(rhs.edges))	{
		rhs.shape = SpawnShape::point;
		rhs.side = SpawnEllipseSide::both;
		rhs.edges = false;
	}
	//copy assignment operator
	SpawnShapeValue& SpawnShapeValue::operator=(const SpawnShapeValue& rhs)
	{
		SpawnShapeValue tmp(rhs);
		*this = std::move(tmp);
		return *this;
	}

	//move assignment operator
	SpawnShapeValue& SpawnShapeValue::operator=(SpawnShapeValue&& rhs)
	{
		std::swap(shape, rhs.shape);
		std::swap(side, rhs.side);
		std::swap(edges, rhs.edges);
		return *this;
	}

	SpawnShape SpawnShapeValue::getShape() {
		return shape;
	}

	void SpawnShapeValue::setShape(SpawnShape shape) {
		this->shape = shape;
	}

	bool SpawnShapeValue::isEdges() {
		return edges;
	}

	void SpawnShapeValue::setEdges(bool edges) {
		this->edges = edges;
	}

	SpawnEllipseSide SpawnShapeValue::getSide() {
		return side;
	}

	void SpawnShapeValue::setSide(SpawnEllipseSide side) {
		this->side = side;
	}


	void SpawnShapeValue::load(const SpawnShapeValue& value) {
		ParticleValue::load(value);
		shape = value.shape;
		edges = value.edges;
		side = value.side;
	}

	void SpawnShapeValue::load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		ParticleValue::load(params,alwaysActive);
		std::string shape_type = string_utils::trim(map_utils::get_const_map_value_string(params, "shape"));
		if (shape_type.compare("point") == 0)
			shape = SpawnShape::point;
		if (shape_type.compare("ellipse") == 0)
			shape = SpawnShape::ellipse;
		if (shape_type.compare("line") == 0)
			shape = SpawnShape::line;
		if (shape_type.compare("square") == 0)
			shape = SpawnShape::square;


		if (shape != SpawnShape::ellipse)
			return;

		//load ellipse parameters
		std::string edges_value = string_utils::trim(map_utils::get_const_map_value_string(params,"edges"));
		if (edges_value.compare("true") == 0)
			edges = true;

		std::string ellipse_side = string_utils::trim(map_utils::get_const_map_value_string(params, "side"));
		if (ellipse_side.compare("both") == 0)
			side = SpawnEllipseSide::both;
		if (ellipse_side.compare("top") == 0)
			side = SpawnEllipseSide::top;
		if (ellipse_side.compare("bottom") == 0)
			side = SpawnEllipseSide::bottom;

	}

	SpawnShapeValue SpawnShapeValue::create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		SpawnShapeValue value;
		value.load(params,alwaysActive);
		return value;
	}
}