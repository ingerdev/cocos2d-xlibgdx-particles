#include <cstdlib>
#include "RangedNumericValue.h"
#include "Utils.h"

namespace particles
{
	//standart constructor
	RangedNumericValue::RangedNumericValue() : lowMin(0.0), lowMax(0.0){}



	float RangedNumericValue::newLowValue() {
		float result = lowMin + (lowMax - lowMin) * CCRANDOM_0_1();
		return result;
	}

	void RangedNumericValue::setLow(float value) {
		lowMin = value;
		lowMax = value;
	}

	void RangedNumericValue::setLow(float min, float max) {
		lowMin = min;
		lowMax = max;
	}

	float RangedNumericValue::getLowMin() {
		return lowMin;
	}

	void RangedNumericValue::setLowMin(float lowMin) {
		lowMin = lowMin;
	}

	float RangedNumericValue::getLowMax() {
		return lowMax;
	}

	void RangedNumericValue::setLowMax(float lowMax) {
		lowMax = lowMax;
	}


	void RangedNumericValue::load(const RangedNumericValue& value) {
		ParticleValue::load(value);
		lowMax = value.lowMax;
		lowMin = value.lowMin;
	}

	void RangedNumericValue::load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		ParticleValue::load(params,alwaysActive);
		lowMin = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "lowMin")));
		lowMax = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "lowMax")));
	}

	RangedNumericValue RangedNumericValue::create(const std::unordered_map<std::string, std::string>& params,bool alwaysActive)
	{
		RangedNumericValue value;	
		value.load(params,alwaysActive);
		return value;
	}
}