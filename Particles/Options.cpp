#include "Options.h"
#include "Utils.h"

namespace particles
{
	Options Options::create(const std::unordered_map<std::string, std::string>& params)
	{
		Options options;
		options.additive = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "additive")));
		options.aligned = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "aligned")));
		options.attached = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "attached")));
		options.behind = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "behind")));
		options.continuous = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "continuous")));
		return options;
	}
}