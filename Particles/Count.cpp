#include "Count.h"
#include "Utils.h"

namespace particles
{
	Count Count::create(const std::unordered_map<std::string, std::string>& params)
	{
		Count count;
		count.min = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "min")));
		count.max = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "max")));
		return count;
	}
}