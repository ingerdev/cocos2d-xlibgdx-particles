#pragma once

namespace particles
{
	class ParticleValue
	{
	public:
		bool active;
		bool alwaysActive;

		//standart constructor
		ParticleValue();
		//copy constructor
		ParticleValue(const ParticleValue& rhs);
		//move constructor
		ParticleValue(ParticleValue&& rhs);
		//copy assignment operator
		ParticleValue& operator=(const ParticleValue& rhs);

			//move assignment operator
		ParticleValue& operator=(ParticleValue&& rhs);

		void setAlwaysActive(bool alwaysActive);

		bool isAlwaysActive();

		bool isActive();

		void setActive(bool active);

		void load(const ParticleValue& value);

		void load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);
	};
}