#include <stdexcept>
#include <string.h>
#include "ParticleFileConfigParser.h"
#include "Utils.h"

namespace particles
{
	std::unordered_map<std::string, ParticleSystemDescription> ParticleFile::parse_file(std::string file)
	{
		std::unordered_map<std::string, ParticleSystemDescription> map;

		//named particles from unified file		
		auto named_parts = get_particle_parts(file);

		for (auto part : named_parts)
		{
			map[part.first] = generate_particle_description(parse_file_record(part.second));
		}
		
		return map;
	}


	std::unordered_map<std::string, std::vector<std::string>> ParticleFile::get_particle_parts(std::string filename)
	{
		std::unordered_map<std::string, std::vector<std::string>> namedParts;
		//List<String> fileLines = System.IO.File.ReadAllLines(fileName).ToList();
		
		std::vector<std::string> fileLines = file_utils::read_file(filename);

		//status of parsing cursor
		//true - we are inside block of 
		//values belongs to one of file parts
		//false - we are between file parts and eating space lines until first non-space
		bool cursorInsideBlock = false;

		int startBlock = 0;
		for (size_t k = 0; k < fileLines.size(); k++)
		{
			//if (String.IsNullOrWhiteSpace(fileLines[k]))
			if (fileLines[k].find_first_not_of(' ') == std::string::npos)
			{
				//if we are out of the block - skip space line
				if (!cursorInsideBlock)
					continue;
				
				//we found end of current file Part
				//yield return new KeyValuePair<string, List<string>>(fileLines[startBlock],
				//	fileLines.GetRange(startBlock, k - startBlock + 1)); 											
				namedParts[fileLines[startBlock]] = std::vector<std::string>(fileLines.begin() + startBlock,
					fileLines.begin() + k);
				

				cursorInsideBlock = false;
			}
			else
			{//we're have non-empty string

				//we found first non-empty string
				if (!cursorInsideBlock)
				{
					cursorInsideBlock = true;
					startBlock = k;
				}
				//check its last line in the fileLines
				//and complete block if so
				if (k + 1 == fileLines.size())
					//yield return new KeyValuePair<string, List<string>>(fileLines[startBlock],
					//fileLines.GetRange(startBlock, k - startBlock + 1));
					namedParts[fileLines[startBlock]] = std::vector<std::string>(fileLines.begin() + startBlock,
					fileLines.begin() + k+1);
			}
			
		}
		return namedParts;
	}

	//filepart to sections aand sections to key-values
	// fileName
	// - SectionName -
	// key : file
	// ....
	// key : file
	std::unordered_map<std::string, std::unordered_map<std::string, std::string>>
		ParticleFile::parse_file_record(std::vector<std::string> file)
	{
		if (file.empty())
			throw std::invalid_argument("invalid file part format - empty");

		//file[0] mean start of first block so it should be named
		if (string_utils::trim(file[0]).find_first_of(" ")!=std::string::npos)
			throw std::invalid_argument("invalid file name (contain spaces)");

		std::unordered_map<std::string, std::unordered_map<std::string, std::string>> file_sections;		

		std::unordered_map<std::string, std::string> section;
		std::string section_name;
		for (auto it = file.begin() + 1; it != file.end();++it)
		{
			//finish current section and start new
			if (is_line_section_header(*it))
			{
				if (!section_name.empty())
				{
					file_sections[section_name]=section;
					
				}
				section.clear();
				section_name = extract_section_name(*it);
				continue;
			}

			//parse section's key:value
			auto entry = parse_single_entry(*it);
			section[entry.first] = entry.second;
		}
		file_sections[section_name] = section;
		return file_sections;
	}

	// "- name -" -> "name"
	std::string ParticleFile::extract_section_name(const std::string& line)
	{
		int leftIndex = line.find_first_of("- ") + 2;
		int rightIndex = line.find_last_of(" -");
		return string_utils::trim(line.substr(leftIndex, rightIndex -leftIndex -1));
	}

	//"key:value" -> "key", "value"
	std::pair<std::string, std::string> ParticleFile::parse_single_entry(std::string entry)
	{
		auto parts = string_utils::split(entry,":");
		//if it isnt key:value pair, make pair with key equal "no_key"
		return (parts.size() < 2) ?
			std::make_pair("no_key",string_utils::trim(parts[0])):
			std::make_pair(string_utils::trim(parts[0]), string_utils::trim(parts[1]));
	}
	//examine string and determine is it Section header or not
	//section header format: "- SectionHEaderName -"
	bool ParticleFile::is_line_section_header(std::string line)
	{
		line = string_utils::trim(line);
		return (string_utils::starts_with(line, "- ") && string_utils::ends_with(line," -"));
	}
	
	/**
	input format: "n1,n2,n3...nN:k1,k2,k3,...kN"
	where n - live time precentage from 0 to 1 (increasing)
	k - absolute value.
	*/
	std::vector<TimelinePair> ParticleFile::parse_timeline(const std::string& input)
	{
		std::vector<TimelinePair> pairs;

		if (input.length() == 0)
			return pairs;

		//take times and values parts
		std::vector<std::string> parts = string_utils::split(input, ":");
		if (parts.size() != 2)
			throw std::invalid_argument("Invalid input string format: no time and value section");

		std::vector<std::string> times = string_utils::split(parts.at(0), ";");
		std::vector<std::string> values = string_utils::split(parts.at(1), ";");
		if (times.size() != values.size())
			throw std::invalid_argument("Invalid input string format: times and values counts does not match");

		for (size_t k = 0; k < times.size(); ++k)
		{			
			pairs.push_back(TimelinePair(std::stof(times[k]), std::stof(values[k])));
		}
		return pairs;
	}


	ParticleSystemDescription ParticleFile::generate_particle_description(const std::unordered_map<std::string, std::unordered_map<std::string, std::string>>& raw_sections)
	{
		ParticleSystemDescription particle;
		//rewrite to table map:function lookup
		for (auto section : raw_sections)
		{
			auto section_name = section.first;
			if (!_stricmp("Delay", section_name.c_str()))
			{
				particle.DelayValue = parse_delay(section.second);
			}else
			if (!_stricmp("LifeOffset", section_name.c_str()))
			{
				particle.LifeOffsetValue = parse_life_offset(section.second);
			}else
			if (!_stricmp("Duration", section_name.c_str()))
			{
				particle.DurationValue = parse_duration(section.second);
			}else
			if (!_stricmp("Life", section_name.c_str()))
			{
				particle.LifeValue = parse_life(section.second);
			}else
			if (!_stricmp("Emission", section_name.c_str()))
			{
				particle.EmissionValue = parse_emission(section.second);
			}else
			if (!_stricmp("Scale", section_name.c_str()))
			{
				particle.ScaleValue = parse_scale(section.second);
			}else
			if (!_stricmp("Rotation", section_name.c_str()))
			{
				particle.RotationValue = parse_rotation(section.second);
			}else
			if (!_stricmp("Velocity", section_name.c_str()))
			{
				particle.VelocityValue = parse_velocity(section.second);
			}else
			if (!_stricmp("Angle", section_name.c_str()))
			{
				particle.AngleValue = parse_angle(section.second);
			}else
			if (!_stricmp("Wind", section_name.c_str()))
			{
				particle.WindValue = parse_wind(section.second);
			}else
			if (!_stricmp("Gravity", section_name.c_str()))
			{
				particle.GravityValue = parse_gravity(section.second);
			}else
			if (!_stricmp("Transparency", section_name.c_str()))
			{
				particle.TransparencyValue = parse_transparency(section.second);
			}else
			if (!_stricmp("Tint", section_name.c_str()))
			{
				particle.TintValue = parse_tint(section.second);
			}else
			if (!_stricmp("X Offset", section_name.c_str()))
			{
				particle.XoffsetValue = parse_x_offset(section.second);
			}else
			if (!_stricmp("Y Offset", section_name.c_str()))
			{
				particle.YoffsetValue = parse_y_offset(section.second);
			}else
			if (!_stricmp("Spawn Width", section_name.c_str()))
			{
				particle.SpawnWidthValue = parse_spawn_width(section.second);
			}else
			if (!_stricmp("Spawn Height", section_name.c_str()))
			{
				particle.SpawnHeightValue = parse_spawn_height(section.second);
			}else
			if (!_stricmp("Spawn Shape", section_name.c_str()))
			{
				particle.SpawnShapeValue = parse_spawn_shape(section.second);
			}else
			if (!_stricmp("Options", section_name.c_str()))
			{
				 init_options(particle,section.second);
			}else
			if (!_stricmp("Count", section_name.c_str()))
			{
				init_count(particle,section.second);
			}else
			if (!_stricmp("Image Path", section_name.c_str()))
			{
				init_name(particle, section.second);
			}
		}
		return particle;
	}


	RangedNumericValue ParticleFile::parse_delay(const std::unordered_map<std::string, std::string>& params)
	{
		return RangedNumericValue::create(params,false);
		
	}
	ScaledNumericValue ParticleFile::parse_life_offset(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params,false);
	}
	RangedNumericValue ParticleFile::parse_duration(const std::unordered_map<std::string, std::string>& params)
	{
		return RangedNumericValue::create(params, true);
	}
	ScaledNumericValue ParticleFile::parse_life(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	ScaledNumericValue ParticleFile::parse_emission(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	ScaledNumericValue ParticleFile::parse_scale(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	ScaledNumericValue ParticleFile::parse_rotation(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_velocity(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_angle(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_wind(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_gravity(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_transparency(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	GradientColorValue ParticleFile::parse_tint(const std::unordered_map<std::string, std::string>& params)
	{
		return GradientColorValue::create(params, false);
	}
	RangedNumericValue ParticleFile::parse_x_offset(const std::unordered_map<std::string, std::string>& params)
	{
		return RangedNumericValue::create(params, false);
	}
	RangedNumericValue ParticleFile::parse_y_offset(const std::unordered_map<std::string, std::string>& params)
	{
		return RangedNumericValue::create(params, false);
	}
	ScaledNumericValue ParticleFile::parse_spawn_width(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	ScaledNumericValue ParticleFile::parse_spawn_height(const std::unordered_map<std::string, std::string>& params)
	{
		return ScaledNumericValue::create(params, true);
	}
	SpawnShapeValue ParticleFile::parse_spawn_shape(const std::unordered_map<std::string, std::string>& params)
	{
		return SpawnShapeValue::create(params, true);
	}
	void ParticleFile::init_options(ParticleSystemDescription& desc, const std::unordered_map<std::string, std::string>& params)
	{
		desc.additive = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "additive")));
		desc.aligned = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "aligned")));
		desc.attached = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "attached")));
		desc.behind = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "behind")));
		desc.continuous = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "continuous")));
		desc.premultipliedAlpha = string_utils::stobool(string_utils::trim(map_utils::get_const_map_value_string(params, "premultipliedAlpha")));		
	}
	void ParticleFile::init_count(ParticleSystemDescription& desc, const std::unordered_map<std::string, std::string>& params)
	{
		desc.particles_count_min= string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "min")));
		desc.particles_count_max = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "max")));
	}

	void ParticleFile::init_name(ParticleSystemDescription& desc, const std::unordered_map<std::string, std::string>& params)
	{
		//filename is no_key single value in - Image path - section
		desc.filename = string_utils::trim(map_utils::get_const_map_value_string(params, "no_key"));		
	}





}