#pragma once
namespace particles
{
	//not used anymore
	class Options
	{
	public:
		bool attached;
		bool continuous;
		bool aligned;
		bool additive;
		bool behind;

		static Options create(const std::unordered_map<std::string, std::string>& params);
	};
}