﻿/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2009      Leonardo Kasperavičius
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
#ifndef __LIBGDX_PARTICLE_SYSTEM_QUAD_H__
#define __LIBGDX_PARTICLE_SYSTEM_QUAD_H__

#include "LibgdxParticleSystem.h"
#include "renderer/CCQuadCommand.h"



/**
* @addtogroup particle_nodes
* @{
*/

/** @brief LibgdxParticleSystemQuad is a subclass of ParticleSystem

It includes all the features of ParticleSystem.

Special features and Limitations:
- Particle size can be any float number.
- The system can be scaled
- The particles can be rotated
- It supports subrects
- It supports batched rendering since 1.1
@since v0.8
*/
namespace particles
{
	class cocos2d::SpriteFrame;
	class cocos2d::EventCustom;

	class  LibgdxParticleSystemQuad : public LibgdxParticleSystem
	{
	public:

		/** creates a Particle Emitter */
		static LibgdxParticleSystemQuad * create();
		/** creates a Particle Emitter with a number of particles */
		static LibgdxParticleSystemQuad * createWithTotalParticles(int numberOfParticles);
		
		static LibgdxParticleSystemQuad* createWithDescription(const std::string& name, const ParticleSystemDescription& description);

		/** Sets a new SpriteFrame as particle.
		WARNING: this method is experimental. Use setTextureWithRect instead.
		@since v0.99.4
		*/
		void setDisplayFrame(SpriteFrame *spriteFrame);

		/** Sets a new texture with a rect. The rect is in Points.
		@since v0.99.4
		* @js NA
		* @lua NA
		*/
		void setTextureWithRect(Texture2D *texture, const Rect& rect);

		/** listen the event that renderer was recreated on Android/WP8
		* @js NA
		* @lua NA
		*/
		void listenRendererRecreated(EventCustom* event);

		/**
		* @js NA
		* @lua NA
		*/
		virtual void setTexture(Texture2D* texture) override;
		/**
		* @js NA
		* @lua NA
		*/
		virtual void updateQuadWithParticle(tParticle* particle, const Vec2& newPosition) override;
		/**
		* @js NA
		* @lua NA
		*/
		virtual void postStep() override;
		/**
		* @js NA
		* @lua NA
		*/
		virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;

		/**
		* @js NA
		* @lua NA
		*/
		virtual void setBatchNode(LibgdxParticleBatchNode* batchNode) override;
		/**
		* @js NA
		* @lua NA
		*/
		virtual void setTotalParticles(int tp) override;

		virtual std::string getDescription() const override;

	CC_CONSTRUCTOR_ACCESS:
		/**
		* @js ctor
		*/
		LibgdxParticleSystemQuad();
		/**
		* @js NA
		* @lua NA
		*/
		virtual ~LibgdxParticleSystemQuad();

		// Overrides
		/**
		* @js NA
		* @lua NA
		*/
		virtual bool initWithTotalParticles(int numberOfParticles) override;

	protected:
		/** initializes the indices for the vertices*/
		void initIndices();

		/** initializes the texture with a rectangle measured Points */
		void initTexCoordsWithRect(const Rect& rect);

		/** Updates texture coords */
		void updateTexCoords();

		void setupVBOandVAO();
		void setupVBO();
		bool allocMemory();

		V3F_C4B_T2F_Quad    *_quads;        // quads to be rendered
		GLushort            *_indices;      // indices
		GLuint              _VAOname;
		GLuint              _buffersVBO[2]; //0: vertex  1: indices

		QuadCommand _quadCommand;           // quad command

	private:
		CC_DISALLOW_COPY_AND_ASSIGN(LibgdxParticleSystemQuad);
	};

	// end of particle_nodes group
	/// @}

	
}
#endif //__LIBGDX_PARTICLE_SYSTEM_QUAD_H__
