#include "Utils.h"
#include <fstream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include "CCFileUtils.h"

namespace string_utils
{
	std::vector<std::string> split(std::string  input, const std::string&  delimiter)
	{
		std::vector<std::string> parts;
		size_t pos = 0;
		std::string token;
		while ((pos = input.find(delimiter)) != std::string::npos) {
			token = input.substr(0, pos);
			parts.push_back(token);
			input.erase(0, pos + delimiter.length());
		}
		parts.push_back(input);

		return parts;
	}

	//note: trim functions does not modify original line so each function mean at least one copying

	// trim from start
	std::string ltrim(std::string s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	// trim from end
	std::string rtrim(std::string s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	// trim from both ends
	std::string trim(std::string s) {
		return ltrim(rtrim(s));
	}

	bool starts_with(std::string const & value, std::string const & starting)
	{
		if (starting.size() > value.size()) return false;
		return std::equal(starting.begin(), starting.end(), value.begin());
	}

	bool ends_with(std::string const & value, std::string const & ending)
	{
		if (ending.size() > value.size()) return false;
		return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
	}

	//takes string contains float number and return float or 0 if not a number
	float stof_without_exceptions(const std::string&  input)
	{
		float result;
		try
		{
			result = std::stof(string_utils::trim(input));
		}
		catch (...)
		{
			return 0;
		}
		return result;
	}
	//takes string contains int number and return float or 0 if not a number
	int stod_without_exceptions(const std::string&  input)
	{
		float result;
		try
		{
			result = std::stod(string_utils::trim(input));
		}
		catch (...)
		{
			return 0;
		}
		return result;
	}

	//takes string contains float number and return float or 0 if not a number
	bool stobool(std::string input)
	{
		return string_utils::trim(input).compare("true") == 0;
		
	}
	

}

namespace file_utils
{
	std::vector<std::string> read_file(const std::string& filename)
	{
		std::ifstream stream(cocos2d::FileUtils::getInstance()->fullPathForFilename(filename));
		if (!stream.good())
		{
			throw new std::invalid_argument("cannot open file");
		}

		std::vector<std::string> content;
		std::string line;
		while (std::getline(stream, line))
		{
			content.push_back(line);
		}
		return content;

	}
}

namespace map_utils
{
	//const unordered_map cannot use indexer (it changes map)
	//so helper function was written to access const map
	std::string get_const_map_value_string(const std::unordered_map<std::string,std::string>& map,const std::string& key)
	{
		auto it = map.find(key);
		if (it != map.end())
			return it->second;

		return std::string("");
	}

}


namespace templated_helpers
{

}