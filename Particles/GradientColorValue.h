#pragma once
#include "base/ccTypes.h"
#include "ParticleValue.h"

namespace particles
{

	//we have only 2 types of timelined pairs so no template yet
	struct TimelineColorPair
	{
		float time;
		//note: Color4F components range is from 0 to 1	
		cocos2d::Color4F color;
		TimelineColorPair(float time, cocos2d::Color4F color)
		{
			this->time = time;
			this->color = color;
		}
	};

	class GradientColorValue :ParticleValue
	{
	private:			
		std::vector<TimelineColorPair> timeline;


	public:
		//standart constructor
		GradientColorValue(); 
	

		std::vector<TimelineColorPair> getTimeline();

		void setTimeline(std::vector<TimelineColorPair> timeline);

		inline bool TimelinePresent()
		{
			return (!timeline.size() != 0);
		}

		cocos2d::Color4F getColor(float percent);
	

		void load(const GradientColorValue& value);

		void load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);

		static GradientColorValue create(const std::unordered_map<std::string, std::string>& params,bool alwaysActive);
	};
}