#include "LibgdxParticleSystem.h"
#include "LibgdxParticleBatchNode.h"
#include "utils.h"
#include <cmath>
#include <algorithm>
#include <ui/CocosGUI.h>

namespace particles
{
	
	LibgdxParticleSystem::LibgdxParticleSystem() :
		  _name("") 

		, _isAutoRemoveOnFinish(false)
		, _sourcePosition(Vec2::ZERO)	
		, _elapsed(0)
		, _duration(0)
		, _max_duration(1)
		, _particleCount(0)
		, _particleIdx(0)
		, _emission(0)
		, _emissionDiff(0)
		, _emissionDelta(0)
		, _lifeOffset(0)
		, _lifeOffsetDiff(0)
		, _life(0)
		, _lifeDiff(0)
		, _spawnWidth(0)
		, _spawnWidthDiff(0)
		, _spawnHeight(0)
		, _spawnHeightDiff(0)		
		, _delay_elapsed(0)
		, _delay_duration(0)
		, _atlasIndex(0)
		, _transformSystemDirty(false)
		, _allocatedParticles(0)
		, _texture(nullptr)
		, _blendFunc(BlendFunc::ALPHA_PREMULTIPLIED)
		, _opacityModifyRGB(false)
		, _isActive(true)
		, _particles(nullptr)
		, _batchNode(nullptr)

	{

	}

	LibgdxParticleSystem::~LibgdxParticleSystem()
	{
		delete _particles;
	}

	LibgdxParticleSystem* LibgdxParticleSystem::createWithDescription(
		const std::string& name,
		const ParticleSystemDescription& description
		)
	{
		LibgdxParticleSystem* ret = new(std::nothrow) LibgdxParticleSystem();				
				
		if (ret&& ret->initWithDescription(name,description))
		{
			ret->autorelease();
			return ret;
		}
		CC_SAFE_DELETE(ret);
		return ret;
	}


	LibgdxParticleSystem* LibgdxParticleSystem::createWithTotalParticles(int numberOfParticles)
	{
		LibgdxParticleSystem *ret = new (std::nothrow) LibgdxParticleSystem();
		if (ret && ret->initWithTotalParticles(numberOfParticles))
		{
			ret->autorelease();
			return ret;
		}
		CC_SAFE_DELETE(ret);
		return ret;
	}


	bool LibgdxParticleSystem::initWithDescription(const std::string& name, const ParticleSystemDescription& description)
	{
		
		_name = name;
		_description = description;
		_opacityModifyRGB = _description.premultipliedAlpha;

		bool ret = false;
		if (initWithTotalParticles(_description.particles_count_max))
		{
			//don't get the internal texture if a batchNode is used
			if (!_batchNode)
			{
				// Set a compatible default for the alpha transfer
				_opacityModifyRGB = false;

				// texture        
				// Try to get the texture from the cache
				std::string textureName = _description.filename;

				size_t rPos = textureName.rfind('/');
				std::string dirname = "";
				if (rPos != std::string::npos)
				{
					std::string textureDir = textureName.substr(0, rPos + 1);

					if (!dirname.empty() && textureDir != dirname)
					{
						textureName = textureName.substr(rPos + 1);
						textureName = dirname + textureName;
					}
				}
				else if (!dirname.empty() && !textureName.empty())
				{
					textureName = dirname + textureName;
				}

				Texture2D *tex = nullptr;

				if (textureName.length() > 0)
				{
					// set not pop-up message box when load image failed
					bool notify = FileUtils::getInstance()->isPopupNotify();
					FileUtils::getInstance()->setPopupNotify(false);
					tex = Director::getInstance()->getTextureCache()->addImage(textureName);
					// reset the value of UIImage notify
					FileUtils::getInstance()->setPopupNotify(notify);
				}

				if (tex)
				{
					setTexture(tex);
				}
				
			}
			ret = true;
		}
		return ret;
	}

	 

	bool LibgdxParticleSystem::addParticle()
	{
		if (this->isFull())
		{
			return false;
		}

		tParticle * particle = &_particles[_particleCount];
		this->initParticle(particle);
		++_particleCount;

		return true;
	}

	bool LibgdxParticleSystem::initWithTotalParticles(int numberOfParticles)
	{		

		CC_SAFE_FREE(_particles);

		_particles = (tParticle*)calloc(_description.particles_count_max, sizeof(tParticle));

		if (!_particles)
		{
			CCLOG("Particle system: not enough memory");
			this->release();
			return false;
		}

		if (_batchNode)
		{
			for (size_t i = 0; i < _description.particles_count_max; i++)
			{
				_particles[i].atlasIndex = i;
			}
		}
		// default, active
		_isActive = true;
		restart();
		_allocatedParticles = numberOfParticles;
		// default blend function
		_blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;

	

		// default: modulate
		// FIXME:: not used
		//    colorModulate = YES;

		_isAutoRemoveOnFinish = false;

		// Optimization: compile updateParticle method
		//updateParticleSel = @selector(updateQuadWithParticle:newPosition:);
		//updateParticleImp = (CC_UPDATE_PARTICLE_IMP) [self methodForSelector:updateParticleSel];
		//for batchNode
		_transformSystemDirty = false;


		return true;
	}


	bool LibgdxParticleSystem::isFull()
	{
		return (_particleCount == _description.particles_count_max);
	}

	void LibgdxParticleSystem::initParticle(tParticle* particle)
	{		
		//takes overall emitter spent life (in percents)
		float percent = _duration / float(_max_duration);
		std::set<UpdateFlags> updateFlags = _updateFlags;

		//life
		particle->currentLife = particle->life = _life + int(_lifeDiff * _description.LifeValue.getScale(percent));

		//velocity
		if (_description.VelocityValue.active) 
		{
			particle->velocity = _description.VelocityValue.newLowValue();
			particle->velocityDiff = _description.VelocityValue.newHighValue();
			if (!_description.VelocityValue.isRelative())
				particle->velocityDiff -= particle->velocity;
		}

		//angle
		particle->angle = _description.AngleValue.newLowValue();
		particle->angleDiff = _description.AngleValue.newHighValue();
		

		if (!_description.AngleValue.isRelative())
			particle->angleDiff -= particle->angle;

		float angle = 0;
		//if we havent changeable angle - calculate sin/cos of start angle and keep it forever in particle
		if (updateFlags.find(UpdateFlags::Angle) == updateFlags.end()) 
		{
			angle = particle->angle + particle->angleDiff * _description.AngleValue.getScale(0);
			particle->angle = angle;
			particle->angleCos = cosf(CC_DEGREES_TO_RADIANS(angle));
			particle->angleSin = sinf(CC_DEGREES_TO_RADIANS(angle));
		}	

		//scale == size
		float spriteWidth = 1;//sprite.getWidth();
		particle->scale = _description.ScaleValue.newLowValue() / spriteWidth;
		particle->start_scale = particle->scale;
		particle->scaleDiff = _description.ScaleValue.newHighValue() / spriteWidth;
		if (!_description.ScaleValue.isRelative()) 
			particle->scaleDiff -= particle->scale;

		
		//sprite-related
		//particle->setScale(particle->scale + particle->scaleDiff * _description.ScaleValue.getScale(0));
		particle->size = particle->scale + particle->scaleDiff * _description.ScaleValue.getScale(0) +10;
		//rotation
		if (_description.RotationValue.active) 
		{
			particle->start_rotation = _description.RotationValue.newLowValue();
			particle->rotationDiff = _description.RotationValue.newHighValue();
			if (!_description.RotationValue.isRelative()) 
				particle->rotationDiff -= particle->rotation;
			float rotation = particle->rotation + particle->rotationDiff * _description.RotationValue.getScale(0);
			if (_description.aligned) 
				rotation += angle;

			//minus because 
			particle->rotation = -rotation;
			//sprite-related
			//particle->setRotation(rotation);
		}

		if (_description.WindValue.active)
		{
			particle->wind = _description.WindValue.newLowValue();
			particle->windDiff = _description.WindValue.newHighValue();
			if (!_description.WindValue.isRelative()) 
				particle->windDiff -= particle->wind;
		}

		if (_description.GravityValue.active) 
		{
			particle->gravity = _description.GravityValue.newLowValue();
			particle->gravityDiff = _description.GravityValue.newHighValue();
			if (!_description.GravityValue.isRelative()) 
				particle->gravityDiff -= particle->gravity;
		}
	
		particle->color = _description.TintValue.getColor(0);
		particle->start_color = particle->color;

		particle->transparency = _description.TransparencyValue.newLowValue();
		particle->transparencyDiff = _description.TransparencyValue.newHighValue() - particle->transparency;
		particle->start_transparency = particle->transparency;
		//!! modified(added) by @ingersol
		// 
		//particle->tint.a = particle->tint.a*particle->transparency;
		particle->color.a = particle->transparency;
		//

		// Spawn.
		float x = _sourcePosition.x;
		if (_description.XoffsetValue.active) 
			x += _description.XoffsetValue.newLowValue();

		float y = _sourcePosition.y;
		if (_description.YoffsetValue.active) 
			y += _description.YoffsetValue.newLowValue();

#pragma region spawnshape
		switch (_description.SpawnShapeValue.shape) 
		{
		case SpawnShape::square: 
		{
			float width = _spawnWidth + (_spawnWidthDiff * _description.SpawnWidthValue.getScale(percent));
			float height = _spawnHeight + (_spawnHeightDiff * _description.SpawnHeightValue.getScale(percent));
			x += width / 2.0 * CCRANDOM_MINUS1_1();
			y += height / 2.0 * CCRANDOM_MINUS1_1();
			break;
		}
		case SpawnShape::ellipse: 
		{
			float width = _spawnWidth + (_spawnWidthDiff * _description.SpawnWidthValue.getScale(percent));
			float height = _spawnHeight + (_spawnHeightDiff * _description.SpawnHeightValue.getScale(percent));
			float radiusX = width / 2.0;
			float radiusY = height / 2.0;
			if (radiusX == 0 || radiusY == 0) break;
			float scaleY = radiusX / (float)radiusY;
			if (_description.SpawnShapeValue.edges)
			{
				float spawnAngle;
				switch (_description.SpawnShapeValue.side)
				{
				case SpawnEllipseSide::top:
					spawnAngle = -179 * CCRANDOM_0_1();
					break;
				case SpawnEllipseSide::bottom:
					spawnAngle = 179 * CCRANDOM_0_1();
					break;
				default:
					spawnAngle = 360 * CCRANDOM_0_1();
					break;
				}
				float a = CC_DEGREES_TO_RADIANS(spawnAngle);
				float cosDeg = cosf(a);
				float sinDeg = sinf(a);
				x += cosDeg * radiusX;
				y += sinDeg * radiusY;
				//calc permanent angle cos/sin
				if (updateFlags.find(UpdateFlags::Angle) == updateFlags.end())
				{
					particle->angle = spawnAngle;
					particle->angleCos = cosDeg;
					particle->angleSin = sinDeg;
				}
			}
			else {
				float radius2 = radiusX * radiusX;
				while (true) {
					float px = width*CCRANDOM_0_1() - radiusX;
					float py = height*CCRANDOM_0_1() - radiusY;
					if (px * px + py * py <= radius2) {
						x += px;
						y += py / scaleY;
						break;
					}
				}
			}
			break;
		}
		case SpawnShape::line: {
			float width = _spawnWidth + (_spawnWidthDiff * _description.SpawnWidthValue.getScale(percent));
			float height = _spawnHeight + (_spawnHeightDiff * _description.SpawnHeightValue.getScale(percent));
			if (width != 0) {
				float lineX = width *CCRANDOM_0_1();
				x += lineX;
				y += lineX * (height / (float)width);
			}
			else
				y += height *CCRANDOM_0_1();
			break;
		}
		default:
			break;
		}
#pragma endregion spawnshape
		//!debug
		//x = 00+ 440 * CCRANDOM_MINUS1_1();
		//y = 600;

		//particle->size = 50;
		//particle->life = 150000000;
		//particle->setBounds(x - spriteWidth / 2, y - spriteHeight / 2, spriteWidth, spriteHeight);
		particle->pos = Vec2(x, y);	
		//particle->rotation =  30;
		

		if (!_description.attached)
		{
			particle->startPos = this->convertToWorldSpace(Vec2::ZERO);
		}
		else
		{
			particle->startPos = _position;
		}

		int offsetTime = (int)(_lifeOffset + _lifeOffsetDiff * _description.LifeOffsetValue.getScale(percent));
		if (offsetTime > 0) {
			if (offsetTime >= particle->currentLife) offsetTime = particle->currentLife - 1;
			//updateParticle(particle, offsetTime / 1000f, offsetTime);
		}
	}


	void LibgdxParticleSystem::onEnter()
	{
#if CC_ENABLE_SCRIPT_BINDING
		if (_scriptType == kScriptTypeJavascript)
		{
			if (ScriptEngineManager::sendNodeEventToJSExtended(this, kNodeOnEnter))
				return;
		}
#endif

		Node::onEnter();

		// update after action in run!
		this->scheduleUpdateWithPriority(1);
	}

	void LibgdxParticleSystem::onExit()
	{
		this->unscheduleUpdate();
		Node::onExit();
	}

	void LibgdxParticleSystem::stopSystem()
	{
		_isActive = false;
		_elapsed = _duration;		
	}

	void LibgdxParticleSystem::resetSystem()
	{
		_isActive = true;
		_elapsed = 0;
		for (_particleIdx = 0; _particleIdx < _particleCount; ++_particleIdx)
		{
			tParticle *p = &_particles[_particleIdx];
			p->currentLife = 0;
			//p->active = false;
		}
	}


	void LibgdxParticleSystem::restart() 
	{
		_delay_duration = _description.DelayValue.active ? _description.DelayValue.newLowValue() : 0;
		_delay_elapsed = 0;

		_elapsed -= _duration;
		_duration = _description.DurationValue.newLowValue();

		_emission = (int)_description.EmissionValue.newLowValue();
		_emissionDiff = (int)_description.EmissionValue.newHighValue();
		if (!_description.EmissionValue.isRelative()) _emissionDiff -= _emission;

		_life = (int)_description.LifeValue.newLowValue();
		_lifeDiff = (int)_description.LifeValue.newHighValue();
		if (!_description.LifeValue.isRelative()) _lifeDiff -= _life;

		_lifeOffset = _description.LifeOffsetValue.active ? (int)_description.LifeOffsetValue.newLowValue() : 0;
		_lifeOffsetDiff = (int)_description.LifeOffsetValue.newHighValue();
		if (!_description.LifeOffsetValue.isRelative()) _lifeOffsetDiff -= _lifeOffset;

		_spawnWidth = _description.SpawnWidthValue.newLowValue();
		_spawnWidthDiff = _description.SpawnWidthValue.newHighValue();
		if (!_description.SpawnWidthValue.isRelative()) _spawnWidthDiff -= _spawnWidth;

		_spawnHeight = _description.SpawnHeightValue.newLowValue();
		_spawnHeightDiff = _description.SpawnHeightValue.newHighValue();
		if (!_description.SpawnHeightValue.isRelative()) _spawnHeightDiff -= _spawnHeight;

		_updateFlags.clear();
		if (_description.AngleValue.active && _description.AngleValue.TimelinePresent()) _updateFlags.insert(UpdateFlags::Angle);
		if (_description.VelocityValue.active) _updateFlags.insert(UpdateFlags::Velocity);
		if (_description.ScaleValue.TimelinePresent()) _updateFlags.insert(UpdateFlags::Scale);
		if (_description.RotationValue.active && _description.RotationValue.TimelinePresent())_updateFlags.insert(UpdateFlags::Rotation);
		if (_description.WindValue.active) _updateFlags.insert(UpdateFlags::Wind);
		if (_description.GravityValue.active) _updateFlags.insert(UpdateFlags::Gravity);
		if (_description.TintValue.TimelinePresent()) _updateFlags.insert(UpdateFlags::Tint);
	}

	void LibgdxParticleSystem::update(float dt)
	{
		CC_PROFILER_START_CATEGORY(kProfilerCategoryParticles, "CCTimelinedParticleSystem - update");
				
		bool stopEmission = false;
		if (_isActive)
		{

			//delay processing
			_delay_elapsed += dt;
			if (_delay_elapsed < _delay_duration)
				stopEmission = true;

			_elapsed += dt;

			//duration processing
			if (_duration < _elapsed)
			{   //cutting emission refresh
				if (!_description.continuous || _allowCompletion)
					stopEmission = true;
				else
					restart();
			}							
		}

		if (!stopEmission)
		{
			_emissionDelta += dt*1000;
			float emissionTime = (_emission + _emissionDiff * _description.EmissionValue.getScale(_elapsed / float(_duration)));

			if (emissionTime > 0) 
			{
				emissionTime = 1000 / emissionTime;
				if (_emissionDelta >= emissionTime)
				{
					int emitCount = int(_emissionDelta / emissionTime);
					emitCount = std::min(emitCount, int(_description.particles_count_max - _particleCount));
					_emissionDelta -= emitCount * emissionTime;
					_emissionDelta %= int(emissionTime);					
					do 
					{
						addParticle();
					} while (--emitCount);					
				}
			}			
		}

		_particleIdx = 0;

		Vec2 currentPosition = Vec2::ZERO;
		//free or relative
		if (_description.attached)
		{

			currentPosition = _position;
		}
		else
		{
			currentPosition = this->convertToWorldSpace(Vec2::ZERO);
		}
		
		Mat4 worldToNodeTM = getWorldToNodeTransform();
		while (_particleIdx < _particleCount)
		{
			tParticle *p = &_particles[_particleIdx];

			// life
			p->currentLife -= dt*1000;
			if (p->currentLife > 0)
			{


				float percent = 1 - p->currentLife / float(p->life);
				std::set<UpdateFlags> updateFlags = _updateFlags;

				if ((updateFlags.find(UpdateFlags::Scale) != updateFlags.end()))
				{
					//!sprite
					//particle->setScale(particle->scale + particle->scaleDiff * _description.ScaleValue.getScale(percent));
					p->scale = p->start_scale + p->scaleDiff * _description.ScaleValue.getScale(percent);
					p->size = p->scale;
				}

				float velocityX(0), velocityY(0);
				if ((updateFlags.find(UpdateFlags::Velocity) != updateFlags.end()))
				{
					float velocity = (p->velocity + p->velocityDiff * _description.VelocityValue.getScale(percent))* dt;


					if ((updateFlags.find(UpdateFlags::Angle) != updateFlags.end()))
					{
						float angle = p->angle + p->angleDiff * _description.AngleValue.getScale(percent);
						velocityX = velocity * cosf(CC_DEGREES_TO_RADIANS(angle));
						velocityY = velocity * sinf(CC_DEGREES_TO_RADIANS(angle));
						if ((updateFlags.find(UpdateFlags::Rotation) != updateFlags.end()))
						{
							float rotation = p->start_rotation + p->rotationDiff * _description.RotationValue.getScale(percent);
							if (_description.aligned)
								rotation += angle;
							//! sprite
							//particle->setRotation(rotation);
							p->rotation = rotation;
						}
					}
					else {
						velocityX = velocity * p->angleCos;
						velocityY = velocity * p->angleSin;
						if (_description.aligned || ((updateFlags.find(UpdateFlags::Rotation) != updateFlags.end())) != 0)
						{
							float rotation = p->start_rotation + p->rotationDiff * _description.RotationValue.getScale(percent);
							if (_description.aligned)
								rotation += p->angle;
							//! sprite
							//particle.setRotation(rotation);
							p->rotation = rotation;
						}
					}

					if ((updateFlags.find(UpdateFlags::Wind) != updateFlags.end()))
						velocityX += (p->wind + p->windDiff * _description.WindValue.getScale(percent)) * dt;

					if ((updateFlags.find(UpdateFlags::Gravity) != updateFlags.end()))
						velocityY += (p->gravity + p->gravityDiff * _description.GravityValue.getScale(percent)) *dt;
					//!sprite
					//particle.translate(velocityX, velocityY);

				}//end of Velocity exist block
				else
				{
					if ((updateFlags.find(UpdateFlags::Rotation) != updateFlags.end()))
					{
						//!sprite
						//particle.setRotation(particle->rotation + particle->rotationDiff * _description.RotationValue.getScale(percent));
						p->rotation = p->start_rotation + p->rotationDiff * _description.RotationValue.getScale(percent);
					}
				}

				Color4F color;
				if ((updateFlags.find(UpdateFlags::Tint) != updateFlags.end()))
					p->color = _description.TintValue.getColor(percent);
				else
					p->color = p->start_color;

				p->color.a = p->transparency + p->transparencyDiff * _description.TransparencyValue.getScale(percent);
				//ignored as processed inside Quad
				///if (_description.premultipliedAlpha) {
				///	float alphaMultiplier = _description.additive ? 0 : 1;
				///	float a = particle->transparency + particle->transparencyDiff * _description.TransparencyValue.getScale(percent);
				//!sprite
				//particle->setColor(color[0] * a, color[1] * a, color[2] * a, a * alphaMultiplier);
				///}
				///else {
				//!sprite
				//particle.setColor(color[0], color[1], color[2],
				//	particle.transparency + particle.transparencyDiff * transparencyValue.getScale(percent));
				///}

				//
				// update values in quad
				//
				//velocityX = -2;
				//velocityY = -15;
				p->pos = p->pos + Vec2(velocityX, velocityY);
				//
				// update values in quad
				//

				Vec2    newPos;

				//free or relative
				if (_description.attached)
				{
					Vec2 diff = currentPosition - p->startPos;
					newPos = p->pos - diff;
				}
				else
				{
					Vec3 p1(currentPosition.x, currentPosition.y, 0), p2(p->startPos.x, p->startPos.y, 0);
					worldToNodeTM.transformPoint(&p1);
					worldToNodeTM.transformPoint(&p2);
					p1 = p1 - p2;
					newPos = p->pos - Vec2(p1.x, p1.y);
				}/*
				if (_positionType == PositionType::FREE)
				{
					Vec3 p1(currentPosition.x, currentPosition.y, 0), p2(p->startPos.x, p->startPos.y, 0);
					worldToNodeTM.transformPoint(&p1);
					worldToNodeTM.transformPoint(&p2);
					p1 = p1 - p2;
					newPos = p->pos - Vec2(p1.x, p1.y);
				}
				else if (_positionType == PositionType::RELATIVE)
				{
					Vec2 diff = currentPosition - p->startPos;
					newPos = p->pos - diff;
				}
				else
				{
					newPos = p->pos;
				}*/

				// translate newPos to correct position, since matrix transform isn't performed in batchnode
				// don't update the particle with the new position information, it will interfere with the radius and tangential calculations
				if (_batchNode)
				{
					newPos.x += _position.x;
					newPos.y += _position.y;
				}

				updateQuadWithParticle(p, newPos);
				//updateParticleImp(self, updateParticleSel, p, newPos);

				// update particle counter
				++_particleIdx;
			}
			//dead particle, removing
			else
			{
				// life < 0
				int currentIndex = p->atlasIndex;
				if (_particleIdx != _particleCount - 1)
				{
					_particles[_particleIdx] = _particles[_particleCount - 1];
				}
				if (_batchNode)
				{
					//disable the switched particle
					_batchNode->disableParticle(_atlasIndex + currentIndex);

					//switch indexes
					_particles[_particleCount - 1].atlasIndex = currentIndex;
				}


				--_particleCount;

				if (_particleCount == 0 && _isAutoRemoveOnFinish)
				{
					this->unscheduleUpdate();
					_parent->removeChild(this, true);
					return;
				}
			}

		}

		CC_PROFILER_STOP_CATEGORY(kProfilerCategoryParticles, "CCTimelinedParticleSystem - update");
	}



	int LibgdxParticleSystem::getTotalParticles() const
	{
		return _description.particles_count_max;
	}

	void LibgdxParticleSystem::setTotalParticles(int var)
	{
		CCASSERT(var <= _allocatedParticles, "Particle: resizing particle array only supported for quads");
		_description.particles_count_max = var;
	}

	void LibgdxParticleSystem::updateWithNoTime(void)
	{
		update(0.0f);
	}

	void LibgdxParticleSystem::updateQuadWithParticle(tParticle* particle, const Vec2& newPosition)
	{
		CC_UNUSED_PARAM(particle);
		CC_UNUSED_PARAM(newPosition);
		// should be overridden
	}

	void LibgdxParticleSystem::postStep()
	{
		// should be overridden
	}

	// TimelinedParticleSystem - Texture protocol
	void LibgdxParticleSystem::setTexture(Texture2D* var)
	{
		if (_texture != var)
		{
			CC_SAFE_RETAIN(var);
			CC_SAFE_RELEASE(_texture);
			_texture = var;
			updateBlendFunc();
		}
	}

	void LibgdxParticleSystem::updateBlendFunc()
	{
		CCASSERT(!_batchNode, "Can't change blending functions when the particle is being batched");

		if (_texture)
		{
			bool premultiplied = _texture->hasPremultipliedAlpha();

			_opacityModifyRGB = false;

			if (_texture && (_blendFunc.src == CC_BLEND_SRC && _blendFunc.dst == CC_BLEND_DST))
			{
				if ((premultiplied) || (_description.premultipliedAlpha))
				{
					_opacityModifyRGB = true;
				}
				else
				{
					_blendFunc = BlendFunc::ALPHA_NON_PREMULTIPLIED;
				}
			}
		}
	}

	Texture2D * LibgdxParticleSystem::getTexture() const
	{
		return _texture;
	}

	const BlendFunc& LibgdxParticleSystem::getBlendFunc() const
	{
		return _blendFunc;
	}

	void LibgdxParticleSystem::setBlendFunc(const BlendFunc &blendFunc)
	{
		if (_blendFunc.src != blendFunc.src || _blendFunc.dst != blendFunc.dst) {
			_blendFunc = blendFunc;
			this->updateBlendFunc();
		}
	}


	LibgdxParticleBatchNode* LibgdxParticleSystem::getBatchNode(void) const
	{
		return _batchNode;
	}

	void LibgdxParticleSystem::setBatchNode(LibgdxParticleBatchNode* batchNode)
	{
		if (_batchNode != batchNode) {

			_batchNode = batchNode; // weak reference

			if (batchNode) {
				//each particle needs a unique index
				for (size_t i = 0; i < _description.particles_count_max; i++)
				{
					_particles[i].atlasIndex = i;
				}
			}

			//CCParticleSystemQuad
		}
	}
	// TimelinedParticleSystem - Additive Blending
	void LibgdxParticleSystem::setBlendAdditive(bool additive)
	{
		if (additive)
		{
			_blendFunc = BlendFunc::ADDITIVE;
		}
		else
		{
			if (_texture && !_texture->hasPremultipliedAlpha())
				_blendFunc = BlendFunc::ALPHA_NON_PREMULTIPLIED;
			else
				_blendFunc = BlendFunc::ALPHA_PREMULTIPLIED;
		}
	}

	bool LibgdxParticleSystem::isBlendAdditive() const
	{
		return(_blendFunc.src == GL_SRC_ALPHA && _blendFunc.dst == GL_ONE);
	}

}