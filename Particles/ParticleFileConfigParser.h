#ifndef __PARTICLE_FILE_CONFIG_PARSER_H__
#define __PARTICLE_FILE_CONFIG_PARSER_H__
#include "RangedNumericValue.h"
#include "ScaledNumericValue.h"
#include "GradientColorValue.h"
#include "SpawnShapeValue.h"
namespace particles
{

	
	struct ParticleSystemDescription
	{
		RangedNumericValue DelayValue;
		ScaledNumericValue LifeOffsetValue;
		RangedNumericValue DurationValue;
		ScaledNumericValue LifeValue;
		ScaledNumericValue EmissionValue;
		ScaledNumericValue ScaleValue;
		ScaledNumericValue RotationValue;
		ScaledNumericValue VelocityValue;
		ScaledNumericValue AngleValue;
		ScaledNumericValue WindValue;
		ScaledNumericValue GravityValue;
		ScaledNumericValue TransparencyValue;		
		GradientColorValue TintValue;
		RangedNumericValue XoffsetValue;
		RangedNumericValue YoffsetValue;
		ScaledNumericValue SpawnWidthValue;
		ScaledNumericValue SpawnHeightValue;				
		SpawnShapeValue SpawnShapeValue;
		
		//Options
		bool attached;
		bool continuous;
		bool aligned;
		bool additive;
		bool behind;
		bool premultipliedAlpha;
		
		
		//Count of particles
		size_t particles_count_min;
		size_t particles_count_max;

		//Filename
		std::string filename;

		ParticleSystemDescription()
		{
			DurationValue.setAlwaysActive(true);
			EmissionValue.setAlwaysActive(true);
			LifeValue.setAlwaysActive(true);
			ScaleValue.setAlwaysActive(true);
			TransparencyValue.setAlwaysActive(true);
			SpawnShapeValue.setAlwaysActive(true);
			SpawnWidthValue.setAlwaysActive(true);
			SpawnHeightValue.setAlwaysActive(true);

			particles_count_min = 0;
			particles_count_max = 4;

			attached = false;
			continuous = false;
			aligned = false;
			behind = false;
			additive = true;
			premultipliedAlpha = false;		
		}
	};

	

	class ParticleFile
	{
	public:
		//one file may contain several particle descriptions
		//so we returning map with key = particle description name
		//and value = ParticleDesctiption
		static std::unordered_map<std::string, ParticleSystemDescription> parse_file(std::string file);
	private:
		//return strings clusters that was divided by empty lines
		//key - name of string cluster (first line)
		//rest - rest of cluster
		static std::unordered_map<std::string, std::vector<std::string>> get_particle_parts(std::string fileName);

		//filepart to sections aand sections to key-values
		// fileName
		// - SectionName -
		// key : file
		// ....
		// key : file
		
		//todo: replace strong types to begin/end iterators?
		static std::unordered_map<std::string, std::unordered_map<std::string, std::string>>
			parse_file_record(std::vector<std::string> file);

		static std::string extract_section_name(const std::string& line);

		static std::pair<std::string, std::string> parse_single_entry(std::string entry);
		static bool is_line_section_header(std::string line);

		static ParticleSystemDescription generate_particle_description(const std::unordered_map<std::string, std::unordered_map<std::string, std::string>>& raw_sections);
		static std::vector<TimelinePair> parse_timeline(const std::string& input);

		static RangedNumericValue parse_delay(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_life_offset(const std::unordered_map<std::string, std::string>& params);
		static RangedNumericValue parse_duration(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_life(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_emission(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_scale(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_rotation(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_velocity(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_angle(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_wind(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_gravity(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_transparency(const std::unordered_map<std::string, std::string>& params);
		static GradientColorValue parse_tint(const std::unordered_map<std::string, std::string>& params);
		static RangedNumericValue parse_x_offset(const std::unordered_map<std::string, std::string>& params);
		static RangedNumericValue parse_y_offset(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_spawn_width(const std::unordered_map<std::string, std::string>& params);
		static ScaledNumericValue parse_spawn_height(const std::unordered_map<std::string, std::string>& params);
		static SpawnShapeValue parse_spawn_shape(const std::unordered_map<std::string, std::string>& params);
		
		static void init_options(ParticleSystemDescription& desc,const std::unordered_map<std::string, std::string>& params);
		static void init_count(ParticleSystemDescription& desc, const std::unordered_map<std::string, std::string>& params);
		static void init_name(ParticleSystemDescription& desc, const std::unordered_map<std::string, std::string>& params);

	};
}
#endif //__PARTICLE_FILE_CONFIG_PARSER_H__