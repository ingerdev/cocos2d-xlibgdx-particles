#ifndef __LIBGDX_PARTICLE_SYSTEM_H__
#define __LIBGDX_PARTICLE_SYSTEM_H__

#include <bitset>
#include "base/CCProtocols.h"
#include "2d/CCNode.h"
#include "base/CCValue.h"
#include "ParticleFileConfigParser.h"

namespace particles
{
	class LibgdxParticleBatchNode;
	using namespace cocos2d;

	typedef struct sParticle
	{		
		//dead or alive
		bool active;
		//max calculated life, current life
		int life, currentLife;
		//current scale, ?
		float start_scale,scale, scaleDiff;
		//current rotation,?
		float start_rotation,rotation, rotationDiff;
		//current velocity, diff?
		float start_velocity,velocity, velocityDiff;
		//current angle, diff?
		//spawn angle (mean sector where they will be directed)
		float angle, angleDiff;
		//cos and sin of current angle
		//saved here/used only when angle update is disabled so we used sin/cos of permanent precalculated angle
		float angleCos, angleSin;
		//current transparency, ?
		float start_transparency,transparency, transparencyDiff;
		//wind, x-force a-la gravity
		float wind, windDiff;
		//gravity,y-force
		float gravity, gravityDiff;
		//color of particle
		Color4F start_color,color;
		//size of particle
		float size;

		//added by @ingersol		
		Vec2     pos,startPos;

		//ParticleSystem-related
		unsigned int    atlasIndex;
			
	} tParticle;	

	enum class UpdateFlags
	{
		Scale,
	    Angle,
		Rotation,
		Velocity,
		Wind,
		Gravity,
		Tint,
	};
	class  LibgdxParticleSystem : public cocos2d::Node, public cocos2d::TextureProtocol
	{
	public:
		LibgdxParticleSystem();
		//name - name of emitter
		//description - set of loaded properties
		static LibgdxParticleSystem* createWithDescription(const std::string& name,const ParticleSystemDescription& description);	
		//! create a system with a fixed number of particles
		static LibgdxParticleSystem* createWithTotalParticles(int numberOfParticles);
		~LibgdxParticleSystem();

		//! should be overridden by subclasses
		virtual void updateQuadWithParticle(tParticle* particle, const Vec2& newPosition);
		//! should be overridden by subclasses
		virtual void postStep();

		/** sourcePosition of the emitter */
		inline const Vec2& getSourcePosition() const { return _sourcePosition; };
		inline void setSourcePosition(const Vec2& pos) { _sourcePosition = pos; };

		/**libgdx properties*/
		inline ScaledNumericValue& get_life(){ return _description.LifeValue; };
		inline ScaledNumericValue& get_scale(){ return _description.ScaleValue; }
		inline ScaledNumericValue& get_rotation(){ return _description.RotationValue; }
		inline GradientColorValue& get_tint(){ return _description.TintValue; }
		inline ScaledNumericValue& get_velocity(){ return _description.VelocityValue; }
		inline ScaledNumericValue& get_wind(){ return _description.WindValue; }
		inline ScaledNumericValue& get_gravity(){ return _description.GravityValue; }
		inline ScaledNumericValue& get_angle(){ return _description.AngleValue; }
		inline ScaledNumericValue& get_emission(){ return _description.EmissionValue; }
		inline ScaledNumericValue& get_transparency(){ return _description.TransparencyValue; }
		inline ScaledNumericValue& get_life_offset(){ return _description.LifeOffsetValue; }
		inline ScaledNumericValue& get_spawn_width(){ return _description.SpawnWidthValue; }
		inline ScaledNumericValue& get_spawn_height(){ return _description.SpawnHeightValue; }
		inline SpawnShapeValue& get_spawn_shape(){ return _description.SpawnShapeValue; }
		inline RangedNumericValue& get_duration(){ return _description.DurationValue; }
		inline RangedNumericValue& get_delay(){ return _description.DelayValue; }
		inline RangedNumericValue& get_x_offset(){ return _description.XoffsetValue; }
		inline RangedNumericValue& get_y_offset(){ return _description.YoffsetValue; }
		inline bool get_attached_property(){ return _description.attached; };
		inline bool get_continuous_property(){ return _description.continuous; };
		inline bool get_additive_property(){ return _description.additive; };
		inline bool get_aligned_property(){ return _description.aligned; };
		inline bool get_behind_property(){ return _description.behind; };
		inline int get_particles_max_count(){ return _description.particles_count_max; };
		inline int get_particles_min_count(){ return _description.particles_count_min; };
		inline void set_attached_property(bool attached){ _description.attached = attached; };
		inline void set_continuous_property(bool continuous){ _description.continuous = continuous; };
		inline void set_additive_property(bool additive) { _description.additive = additive; };
		inline void set_aligned_property(bool aligned){ _description.aligned = aligned; };
		inline void set_behind_property(bool behind){ _description.behind = behind; };
		inline void set_paticles_max_count(int max_count){ _description.particles_count_max = max_count; restart(); };
		inline void set_paticles_min_count(int min_count){ _description.particles_count_min = min_count; restart(); };







		virtual void updateWithNoTime(void);
		//virtual bool isActive() const;
		virtual bool isBlendAdditive() const;
		virtual void setBlendAdditive(bool value);

		virtual LibgdxParticleBatchNode* getBatchNode() const;
		virtual void setBatchNode(LibgdxParticleBatchNode* batchNode);

		// index of system in batch node array
		inline int getAtlasIndex() const { return _atlasIndex; };
		inline void setAtlasIndex(int index) { _atlasIndex = index; };

		/** maximum particles of the system */
		virtual int getTotalParticles() const;
		virtual void setTotalParticles(int totalParticles);

		// Overrides
		virtual void onEnter() override;
		virtual void onExit() override;
		virtual void update(float dt) override;
		virtual cocos2d::Texture2D* getTexture() const override;
		virtual void setTexture(cocos2d::Texture2D *texture) override;
		/**
		*@code
		*When this function bound into js or lua,the parameter will be changed
		*In js: var setBlendFunc(var src, var dst)
		*In lua: local setBlendFunc(local src, local dst)
		*@endcode
		*/
		virtual void setBlendFunc(const BlendFunc &blendFunc) override;
		/**
		* @js NA
		* @lua NA
		*/
		virtual const BlendFunc &getBlendFunc() const override;
	protected:
		bool initWithDescription(const std::string& name, const ParticleSystemDescription& description);
		void restart();
		virtual void updateBlendFunc();

		//emitter name
		std::string _name;
		//emitter properties
		ParticleSystemDescription _description;
		//emitted particles
		tParticle* _particles;


		//! Add a particle to the emitter
		bool addParticle();
		//! Initializes a system with a fixed number of particles
		virtual bool initWithTotalParticles(int numberOfParticles);
		
		//! Initializes a particle
		void initParticle(tParticle* particle);
		//! stop emitting particles. Running particles will continue to run until they die
		void stopSystem();
		//! Kill all living particles.
		void resetSystem();
		//! whether or not the system is full
		bool isFull();

		/** weak reference to the SpriteBatchNode that renders the Sprite */
		LibgdxParticleBatchNode* _batchNode;

		// index of system in batch node array
		int _atlasIndex;
		/** Is the emitter active */
		bool _isActive;

		/** whether or not the node will be auto-removed when it has no particles left.
		By default it is false.
		@since v0.8
		*/
		bool _isAutoRemoveOnFinish;

		/** Quantity of particles that are being simulated at the moment */
		int _particleCount;
		
		// Time elapsed since start (including Delay period)
		float _elapsed;

		float _accumulator;
		//private Sprite sprite;		
		float _x, _y;		

		//!  particle idx
		int _particleIdx;

		/** sourcePosition of the emitter */
		Vec2 _sourcePosition;
				
		bool _firstUpdate;
		bool _flipX, _flipY;
		std::set<UpdateFlags> _updateFlags;
		
		//this flag rule system wait to die all particles on restart process
		bool _allowCompletion;		

		int _emission, _emissionDiff, _emissionDelta;
		int _lifeOffset, _lifeOffsetDiff;
		int _life, _lifeDiff;
		float _spawnWidth, _spawnWidthDiff;
		float _spawnHeight, _spawnHeightDiff;
		float _max_duration, _duration;
		float _delay_elapsed, _delay_duration;
		
		//not used. This parameter resets blend function after each draw on libgdx
		//i see no matches in cocos2d
		bool _cleansUpBlendFunction;

		//true if scaled or rotated
		bool _transformSystemDirty;
		// Number of allocated particles
		int _allocatedParticles;



		/** conforms to CocosNodeTexture protocol */
		Texture2D* _texture;
		/** conforms to CocosNodeTexture protocol */
		BlendFunc _blendFunc;
		/** does the alpha value modify color */
		bool _opacityModifyRGB;
	};
}
#endif //__LIBGDX_PARTICLE_SYSTEM_H__
