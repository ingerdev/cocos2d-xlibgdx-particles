#include "ScaledNumericValue.h"
#include <stdexcept>
#include "Utils.h"

namespace particles
{
	//standart constructor
	ScaledNumericValue::ScaledNumericValue() :highMin(0.0), highMax(0.0), relative(0)
	{
		timeline.push_back(TimelinePair(0, 1));
	}


	float ScaledNumericValue::newHighValue() {
		return highMin + (highMax - highMin) * CCRANDOM_0_1();
	}

	void ScaledNumericValue::setHigh(float value) {
		highMin = value;
		highMax = value;
	}

	void ScaledNumericValue::setHigh(float min, float max) {
		highMin = min;
		highMax = max;
	}

	float ScaledNumericValue::getHighMin() {
		return highMin;
	}

	void ScaledNumericValue::setHighMin(float highMin) {
		highMin = highMin;
	}

	float ScaledNumericValue::getHighMax() {
		return highMax;
	}

	void ScaledNumericValue::setHighMax(float highMax) {
		highMax = highMax;
	}



	std::vector<TimelinePair> ScaledNumericValue::getTimeline() {
		return timeline;
	}

	void ScaledNumericValue::setTimeline(std::vector<TimelinePair> timeline) {
		this->timeline = timeline;
	}

	bool ScaledNumericValue::isRelative() {
		return relative;
	}

	void ScaledNumericValue::setRelative(boolean relative) {
		relative = this->relative;
	}

	float ScaledNumericValue::getScale(float percent) {
		int endIndex = -1;
		std::vector<TimelinePair> timeline = this->timeline;
		size_t n = timeline.size();
		for (size_t i = 1; i < n; i++) {
			float t = timeline[i].time;
			if (t > percent) {
				endIndex = i;
				break;
			}
		}
		if (endIndex == -1) return timeline[n - 1].value;
		int startIndex = endIndex - 1;
		float startValue = timeline[startIndex].value;
		float startTime = timeline[startIndex].time;
		return startValue + (timeline[endIndex].value - startValue) * ((percent - startTime) /
			(timeline[endIndex].time - startTime));
	}

	void ScaledNumericValue::load(const ScaledNumericValue& value) {
		RangedNumericValue::load(value);
		highMax = value.highMax;
		highMin = value.highMin;
		timeline = value.timeline;
		relative = value.relative;
	}

	void ScaledNumericValue::load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		RangedNumericValue::load(params,alwaysActive);

		highMin = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "highMin")));
		highMax = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "highMax")));

		std::string rel = string_utils::trim(map_utils::get_const_map_value_string(params, "relative"));
		relative = (rel.compare("true") == 0);

		//reading scaling
		timeline.clear();
		size_t scaling_count = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "scalingCount")));
		size_t timeline_count = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "timelineCount")));
		if (scaling_count != timeline_count)
			throw std::domain_error("invalid config file. timelines count does not match scaling count");


		for (size_t k = 0; k < scaling_count; ++k)
		{
			std::string param_name_scaling = "scaling" + string_utils::to_string(k);
			std::string param_name_timeline = "timeline" + string_utils::to_string(k);


			float time = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, param_name_timeline)));
			float value = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, param_name_scaling)));
			timeline.push_back(TimelinePair(time, value));
		}
		//check class contract
		if (timeline.empty())
			timeline.push_back(TimelinePair(0, 1));

	}

	ScaledNumericValue ScaledNumericValue::create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		ScaledNumericValue value;
		value.load(params,alwaysActive);
		return value;
	}
}