#include "ParticleValue.h"
#include "Utils.h"

namespace particles
{
	//standart constructor
	ParticleValue::ParticleValue() : active(false), alwaysActive(false){}
	//copy constructor
	ParticleValue::ParticleValue(const ParticleValue& rhs) : active(rhs.active),
		alwaysActive(rhs.alwaysActive){}
	//move constructor
	ParticleValue::ParticleValue(ParticleValue&& rhs) : active(std::move(rhs.active)), alwaysActive(std::move(rhs.alwaysActive))	{
		rhs.active = 0;
		rhs.alwaysActive = 0;
	}
	//copy assignment operator
	ParticleValue& ParticleValue::operator=(const ParticleValue& rhs)
	{
		ParticleValue tmp(rhs);
		*this = std::move(tmp);
		return *this;
	}

	//move assignment operator
	ParticleValue& ParticleValue::operator=(ParticleValue&& rhs)
	{
		std::swap(active, rhs.active);
		std::swap(alwaysActive, rhs.alwaysActive);
		return *this;
	}

	void ParticleValue::setAlwaysActive(bool alwaysActive) {		
		alwaysActive = alwaysActive;
		if (alwaysActive)
			active = true;
	}

	bool ParticleValue::isAlwaysActive() {
		return alwaysActive;
	}

	bool ParticleValue::isActive() {
		return alwaysActive || active;
	}

	void ParticleValue::setActive(bool active) {
		active = active;
	}

	void ParticleValue::load(const ParticleValue& value) {
		active = value.active;
		alwaysActive = value.alwaysActive;
	}

	void ParticleValue::load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		setAlwaysActive(alwaysActive);
		if (!alwaysActive)
			active = string_utils::trim(map_utils::get_const_map_value_string(params, "active")) == "true";
	}
}

