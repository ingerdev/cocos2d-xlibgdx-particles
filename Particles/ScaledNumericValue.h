#pragma once
#include "RangedNumericValue.h"

namespace particles
{
	/**
	Structure that contain timeline values
	*/
	struct TimelinePair
	{
		//time mean % of particle lifetime
		//0..1
		float time;
		float value;

		TimelinePair(float time, float value)
		{
			this->time = time;
			this->value = value;
		}
	};

	class ScaledNumericValue :public RangedNumericValue
	{
	private:
		std::vector<TimelinePair> timeline;
		float highMin, highMax;
		bool relative;
	public:		
		//standart constructor
		ScaledNumericValue();
		
		inline bool TimelinePresent()
		{
			return (timeline.size() > 1);
		}
		float newHighValue();

		void setHigh(float value);

		void setHigh(float min, float max);

		float getHighMin();

		void setHighMin(float highMin);

		float getHighMax();

		void setHighMax(float highMax);

			

		std::vector<TimelinePair> getTimeline();

		void setTimeline(std::vector<TimelinePair> timeline);

		bool isRelative();

		void setRelative(boolean relative);

		float getScale(float percent);

		void load(const ScaledNumericValue& value);

		void load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);

		static ScaledNumericValue create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);
	
	};
}