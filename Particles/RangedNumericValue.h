#pragma once
#include "ParticleValue.h"

namespace particles
{
	class RangedNumericValue:public ParticleValue
	{
	private:
		float lowMin, lowMax;
	public:
		//standart constructor
		RangedNumericValue();
	
		float newLowValue();

		void setLow(float value);

		void setLow(float min, float max);

		float getLowMin();

		void setLowMin(float lowMin);

		float getLowMax();

		void setLowMax(float lowMax);

		
		void load(const RangedNumericValue& value);

		void load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);

		static RangedNumericValue create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);
	};
}