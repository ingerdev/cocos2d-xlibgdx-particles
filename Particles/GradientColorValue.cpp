#include "GradientColorValue.h"
#include "Utils.h"

namespace particles
{
	//standart constructor
	GradientColorValue::GradientColorValue()
	{
		ParticleValue::alwaysActive = true;
		timeline.push_back(TimelineColorPair(0, cocos2d::Color4F::WHITE));
	}
	

	std::vector<TimelineColorPair> GradientColorValue::getTimeline() {
		return timeline;
	}

	void GradientColorValue::setTimeline(std::vector<TimelineColorPair> timeline) {
		this->timeline = timeline;
	}


	cocos2d::Color4F GradientColorValue::getColor(float percent) {
		int startIndex = 0, endIndex = -1;
		std::vector<TimelineColorPair> timeline = this->timeline;
		int n = timeline.size();
		for (int i = 1; i < n; i++) {
			float t = timeline[i].time;
			if (t > percent) {
				endIndex = i;
				break;
			}
			startIndex = i;
		}

		float startTime = timeline[startIndex].time;
		if (endIndex == -1) {
			return timeline[startIndex].color;
		}
		float factor = (percent - startTime) / (timeline[endIndex].time - startTime);

		cocos2d::Color4F temp;
		temp.a = timeline[endIndex].color.a;
		temp.r = timeline[startIndex].color.r + (timeline[endIndex].color.r - timeline[startIndex].color.r) * factor;
		temp.g = timeline[startIndex].color.g + (timeline[endIndex].color.g - timeline[startIndex].color.g) * factor;
		temp.b = timeline[startIndex].color.b + (timeline[endIndex].color.b - timeline[startIndex].color.b) * factor;
		return temp;
	}


	void GradientColorValue::load(const GradientColorValue& value) {
		ParticleValue::load(value);
		timeline = value.timeline;
	}

	void GradientColorValue::load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		ParticleValue::load(params,alwaysActive);

		//reading timeline
		timeline.clear();
		int timeline_count = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "timelineCount")));
		int colors_count = string_utils::stod_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, "colorsCount")));

		if (colors_count / 3 != timeline_count)
			throw std::domain_error("invalid config file. timelines count does not match colors count");

		for (int k = 0; k < timeline_count; ++k)
		{
			std::string param_name_time = "timeline" + string_utils::to_string(k);
			float time = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, param_name_time)));


			std::string r_name = "colors" + string_utils::to_string(k * 3);
			std::string g_name = "colors" + string_utils::to_string(k * 3 + 1);
			std::string b_name = "colors" + string_utils::to_string(k * 3 + 2);

			cocos2d::Color4F temp;
			temp.a = 1;
			temp.r = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, r_name)));
			temp.g = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, g_name)));
			temp.b = string_utils::stof_without_exceptions(string_utils::trim(map_utils::get_const_map_value_string(params, b_name)));
			timeline.push_back(TimelineColorPair(time, temp));
		}

		//check class contract
		if (timeline.empty())
			timeline.push_back(TimelineColorPair(0, cocos2d::Color4F::WHITE));
	}

	GradientColorValue GradientColorValue::create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive)
	{
		GradientColorValue value;
		value.load(params,alwaysActive);
		return value;
	}
}