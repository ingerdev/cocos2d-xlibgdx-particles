#pragma once
#include "ParticleValue.h"

namespace particles
{
	enum class SpawnShape {
		point, line, square, ellipse
	};

	enum class SpawnEllipseSide {
		both, top, bottom
	};
	class SpawnShapeValue :public ParticleValue
	{
	public:
		SpawnShape shape;
		bool edges;
		SpawnEllipseSide side;			
		//standart constructor
		SpawnShapeValue();
		//copy constructor
		SpawnShapeValue(const SpawnShapeValue& rhs);
		//move constructor
		SpawnShapeValue(SpawnShapeValue&& rhs);
		//copy assignment operator
		SpawnShapeValue& operator=(const SpawnShapeValue& rhs);

		//move assignment operator
		SpawnShapeValue& operator=(SpawnShapeValue&& rhs);

		SpawnShape getShape();

		void setShape(SpawnShape shape);

		bool isEdges();

		void setEdges(bool edges);

		SpawnEllipseSide getSide();

		void setSide(SpawnEllipseSide side);

		
		void load(const SpawnShapeValue& value);

		void load(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);

		static SpawnShapeValue create(const std::unordered_map<std::string, std::string>& params, bool alwaysActive);
	};	
}